package cn.sitcat.springbootrestfulcrud.controller;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author hiseico
 * @date 2019/1/26 17:57
 * @desc
 */
@Controller
public class loginController {
    /**
     * @param username
     * @param password
     * @return
     *
     * RequestParam 表示必传参数
     */
    @PostMapping("/user/login")
    public String login(@RequestParam("username") String username, @RequestParam("password") String password, Map<String,Object> msg, HttpSession session){
        if(!StringUtils.isEmpty(username)&& "123".equals(password)){
//            登录成功
            session.setAttribute("loginUser",username);
            return "redirect:/main";
        }else{
            msg.put("error","用户名或密码错误！");
            return "login";
        }
    }
}

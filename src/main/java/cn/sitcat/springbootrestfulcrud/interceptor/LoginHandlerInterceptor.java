package cn.sitcat.springbootrestfulcrud.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author hiseico
 * @date 2019/1/26 18:57
 * @desc 登录拦截器
 * 对没有登录的用户进行拦截
 */
public class LoginHandlerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Object user =request.getSession().getAttribute("loginUser");
        if(user==null){
//            未登录，返回登录页面
            request.setAttribute("error","没有权限，请先登录！");
            request.getRequestDispatcher("/login").forward(request,response);
            return  false;

        }
            return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}

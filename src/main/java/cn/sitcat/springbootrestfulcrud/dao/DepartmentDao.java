package cn.sitcat.springbootrestfulcrud.dao;

import cn.sitcat.springbootrestfulcrud.entities.Department;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class DepartmentDao {

    private static Map<Integer, Department> departments = null;

    static {
        departments = new HashMap<Integer, Department>();

        departments.put(101, new Department(101, "市场部"));
        departments.put(102, new Department(102, "人事部"));
        departments.put(103, new Department(103, "销售部"));
        departments.put(104, new Department(104, "技术部"));
        departments.put(105, new Department(105, "客服部"));
    }

    public Collection<Department> getDepartments() {
        return departments.values();
    }

    public Department getDepartment(Integer id) {
        return departments.get(id);
    }

}

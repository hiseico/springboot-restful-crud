package cn.sitcat.springbootrestfulcrud.config;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * @author hiseico
 * @date 2019/1/26 17:29
 * @desc 可以在链接上携带区域信息
 */
public class MyLocaleResolver implements LocaleResolver {

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        String l = request.getParameter("l");
        Locale locale = null;
        locale = Locale.getDefault();
        if (!StringUtils.isEmpty(l)) {
            String[] arr = l.split("_");
            locale = new Locale(arr[0], arr[1]);
        }

        return locale;

    }

    @Override
    public void setLocale(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Locale locale) {

    }
}
